
from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from models import Company, FiscalYear, Account,\
                        Ledger, Move, MoveLine, Reconcile


class CompanyAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code']
    list_display = ('name', 'code')
    list_filter = ("created", "modified")


class FiscalYearAdmin(admin.ModelAdmin):
    search_fields = ['company__name', 'company__code', 'year']
    list_display = ('company', 'year', 'start', 'end', 'state')
    list_filter = ('company', 'state')


class PeriodAdmin(admin.ModelAdmin):
    search_fields = ['fiscl_year__company__name', 'fiscl_year__company__code',
                'fiscal_year__year']
    list_display = ('company', 'fiscal_year', 'start', 'end', 'active')
    list_filter = ('fiscal_year__company', 'fiscal_year__year', 'active')


class AccountAdmin(MPTTModelAdmin):
    search_fields = ['code', 'name', 'tags__name']
    list_display = ('code', 'company', 'name',
                'balance')
    list_filter = ('company', 'type',
                    'detail', 'reconciliable', 'tags__name')
    readonly_fields = ('credit', 'debit', 'balance')


class LedgerAdmin(admin.ModelAdmin):
    search_fields = ['name', 'host__name', 'code']
    list_display = ('code', 'name', 'type', 'active')
    list_filter = ('type', 'active')


class MoveLineInline(admin.TabularInline):
    model = MoveLine
    fields = ('account', 'reference', 'debit', 'credit')


class MoveAdmin(admin.ModelAdmin):
    search_fields = ['name', 'ledger__name', 'ledege__code']
    list_display = ('date', 'name', 'ledger', 'fiscal_year', 'status')
    list_filter = ('status', 'fiscal_year__company', 'fiscal_year__year')
    date_hierarchy = "date"
    inlines = [
        MoveLineInline,
    ]


class MoveLineAdmin(admin.ModelAdmin):
    search_fields = ['move__name', 'acount__name', 'account__code']
    list_display = ('move', 'account', 'reference',
                     'credit', 'debit')
    #list_filter = (,)
    #date_hierarchy = "move__date"


class MoveLineInline(admin.TabularInline):
    model = MoveLine
    fields = ('account', 'reference', 'debit', 'credit')


class ReconcileAdmin(admin.ModelAdmin):

    inlines = [
        MoveLineInline,
    ]

admin.site.register(Company, CompanyAdmin)
admin.site.register(FiscalYear, FiscalYearAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Ledger, LedgerAdmin)
admin.site.register(Move, MoveAdmin)
admin.site.register(MoveLine, MoveLineAdmin)
admin.site.register(Reconcile, ReconcileAdmin)
