from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models import Sum
from django.core.exceptions import ValidationError
from django.utils.timezone import now

from mptt.models import MPTTModel, TreeForeignKey
from taggit.managers import TaggableManager
from currency_rates.models import Currency, default_currency


# Create your models here.
class Base(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def cache_key(self):
        tablename = self._meta.table
        return "%s:%s:%s" % (tablename, self.id, self.updated)

    def touch(self):
        ##
        self.save()

    @models.permalink
    def get_absolute_url(self):
        url = "%s_detail" % self._meta.object_name.lower()
        return (url, (), {'pk': self.id})


#def descendats(instance)
#   for field_name in instancel._meta.foreign_keys
#       field = getattr(instance, fieldname)
#       field.touch()
class Company(Base):
    code = models.CharField(max_length=10, verbose_name=_("Code"), \
                blank=True, null=True, unique=True)
    name = models.CharField(max_length=100, verbose_name=_("Name"), \
                blank=True, null=True, unique=True)
    currency = models.ForeignKey(Currency, default=default_currency,
                            blank=True, null=True)
    active = models.BooleanField(default=True)
    logo = models.ImageField(upload_to="logos", null=True, blank=True)

    # * partner
    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


FISCAL_YEAR_STATES = (
    ('draft', _('Draft')),
    ('open', _('Open')),
    ('closed', _('Closed')),
)


class FiscalYear(Base):
    company = models.ForeignKey(Company, related_name='fiscal_years')
    year = models.PositiveIntegerField()
    start = models.DateField(blank=True)
    end = models.DateField(blank=True)
    state = models.CharField(max_length=10, choices=FISCAL_YEAR_STATES,
                                default='draft')
    default = models.BooleanField(_('Default'), default=False,
        help_text=_('Make this the default for the company.'))

    def __unicode__(self):
        return "%s %s" % (self.company, self.year)

    class Meta:
        ordering = ('company', '-year')
        unique_together = ('company', 'year')

    def save(self, **kwargs):
        if self.default:
            try:
                default_year = FiscalYear.objects.get(default=True,
                                                        company=self.company)
            except self.DoesNotExist:
                pass
            else:
                default_year.default = False
                default_year.save()
        super(FiscalYear, self).save(**kwargs)


ACCOUNT_TYPES = (
    ('income', _('Income')),
    ('expense', _('Expense')),
    ('asset', _('Asset')),
    ('liability', _('Liability')),
    ('equity', _('Equity')),
)
# ('view', 'View'),
# ('other', 'Regular'),
# ('receivable', 'Receivable'),
# ('payable', 'Payable'),
# ('liquidity','Liquidity'),
# ('consolidation', 'Consolidation'),
# ('closed', 'Closed'),


class AccountType(Base):
    name = models.CharField(max_length=30, verbose_name=_("Name"))
    type = models.CharField(max_length=20, choices=ACCOUNT_TYPES,
                    verbose_name=_("Base type"))


class Account(MPTTModel, Base):
    company = models.ForeignKey(Company, related_name='accounts')
    parent = TreeForeignKey('self', null=True, blank=True,
                                related_name='children')
    code = models.CharField(max_length=10, verbose_name=_("Code"), \
                blank=True, null=True)
    name = models.CharField(max_length=100, verbose_name=_("Name"), \
                blank=True, null=True)
    type = models.CharField(max_length=20, choices=ACCOUNT_TYPES,
                    verbose_name=_("Account type"), default="auto")
    detail = models.BooleanField()
    reconciliable = models.BooleanField()
    seconday_currency = models.ForeignKey(Currency, blank=True, null=True)

    active = models.BooleanField(default=True)

    tags = TaggableManager(blank=True)

    class MPTTMeta:
        order_insertion_by = ['code']

    class Meta:
        unique_together = (('company', 'code'),)

    def clean(self):
        """ Validations """
        if not self.code.startswith(self.parent.code):
            raise ValidationError({'code': 'Incorrect code'})
        if self.parent and (self.parent.company != self.company):
            raise ValidationError({'fiscal_year': 'Incorrect company'})

    @property
    def credit(self):
        return MoveLine.objects.filter(account=self)\
                            .aggregate(Sum('credit'))['credit__sum'] or 0

    @property
    def debit(self):
        return MoveLine.objects.filter(account=self)\
                            .aggregate(Sum('debit'))['debit__sum'] or 0

    @property
    def balance(self):
        return self.debit - self.credit

    def __unicode__(self):
        return self.code


LEDGER_TYPES = (
    ('open', _('Open')),
    ('sale', _('Sale')),
    ('purchase', _('Purchase')),
    ('bank', _('Bank')),
)


class Ledger(Base):
    company = models.ForeignKey(Company, related_name='ledgers')
    code = models.CharField(max_length=10, verbose_name=_("Code"), \
                blank=True, null=True)
    name = models.CharField(max_length=100, verbose_name=_("Name"), \
                blank=True, null=True)
    type = models.CharField(max_length=20, choices=LEDGER_TYPES,
                    verbose_name=_("Ledger type"))
    active = models.BooleanField(default=True)

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.name


MOVE_STATES = (
    ('draft', _('Draft')),
    ('done',  _('Done')),
)


class Move(Base):
    date = models.DateField(default=now)
    name = models.CharField(max_length=100, verbose_name=_("Name"), \
                blank=True, null=True)
    ledger = models.ForeignKey(Ledger, related_name="moves")
    fiscal_year = models.ForeignKey(FiscalYear)
    status = models.CharField(max_length=20, choices=MOVE_STATES,
                    verbose_name=_("Move satete"), default='draft')

    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.name

    @property
    def credit(self):
        return self.lines.aggregate(Sum('credit'))['credit__sum'] or 0

    @property
    def debit(self):
        return self.lines.aggregate(Sum('debit'))['debit__sum'] or 0

    @property
    def balance(self):
        return self.debit - self.credit


class MoveLine(Base):
    move = models.ForeignKey(Move, related_name='lines')
    account = TreeForeignKey(Account, related_name='moves',
                limit_choices_to={'detail': True})
    credit = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    debit = models.DecimalField(max_digits=20, decimal_places=2, default=0)

    due_date = models.DateField(blank=True, null=True)
    reference = models.CharField(max_length=20, blank=True, null=True)
    reconcile = models.ForeignKey("Reconcile", blank=True, null=True,
                    related_name='lines')

    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    currency = models.ForeignKey(Currency, blank=True, null=True)
    amount_currency = models.DecimalField(max_digits=20,
                            decimal_places=2, default=0)

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return "%s %s %s" % (self.account, self.debit, self.credit)

    def clean(self):
        """ Validations """
        if self.move_id:
            if self.account.company != self.move.fiscal_year.company:
                raise ValidationError({'account':
                                    'Incorrect fiscal year account'})

        if self.credit == 0 and self.debit == 0:
            raise ValidationError({'credit':
                                    'Must inform credit or debit'})

        if not self.account.detail:
            raise ValidationError({'account':
                                    'Account does not accept moves'})


RECONCILE_STATES = (
    ('draft', _('Draft')),
    ('done', _('Done')),
)


class Reconcile(Base):
    status = models.CharField(max_length=20, choices=RECONCILE_STATES,
                    verbose_name=_("Reconcile status"))

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return "%s %s %s" % (self.status, self.linea.count())

    def reconcile(self):
        """ validate same account, value = 0 """
        pass
