# Create your views here.

from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect, render, get_object_or_404

from accounting import models
import forms


def home(request):
    return render(request, 'accounting/home.html')


#
# Currencies
#
class CurrencyListView(ListView):
    model = models.Currency
    paginate_by = 20

currencies = CurrencyListView.as_view()


class CurrencyCreateView(CreateView):
    model = models.Currency

currency_create = CurrencyCreateView.as_view()


class CurrencyUpdateView(UpdateView):
    model = models.Currency

currency_detail = CurrencyUpdateView.as_view()


class CurrencyDeleteView(DeleteView):
    model = models.Move
    success_url = reverse_lazy('move_list')

currency_delete = CurrencyDeleteView.as_view()


#
# Companies
#
class CompanyListView(ListView):
    model = models.Company

companies = CompanyListView.as_view()


class CompanyCreateView(CreateView):
    model = models.Company
    form_class = forms.CompanyForm

company_create = CompanyCreateView.as_view()


class CompanyUpdateView(UpdateView):
    model = models.Company
    form_class = forms.CompanyForm

company_detail = CompanyUpdateView.as_view()


class CompanyDeleteView(DeleteView):
    model = models.Company
    success_url = reverse_lazy('company_list')

company_delete = CompanyDeleteView.as_view()


# Create chart of accounts
def company_create_chart(request, pk):
    company = get_object_or_404(models.Company, pk=pk)
    form = forms.CompanyCreateChartForm(request.POST or None)
    if form.is_valid():
        template = form.cleaned_data['template']
        template.create(company)
        return redirect(company.get_absolute_url())

    context = {
        'company': company,
        'form': form,
    }

    return render(request, 'accounting/company_create_chart.html', context)


#
# Fiscal years
#
class FiscaYearCreateView(CreateView):
    model = models.FiscalYear
    form_class = forms.FiscalYearCreateForm

    def get_context_data(self, **kwargs):
        context = super(FiscaYearCreateView, self).get_context_data(**kwargs)
        context['company'] = models.Company.objects.get(pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        company = models.Company.objects.get(pk=self.kwargs['pk'])
        fiscal_year = form.save(commit=False)
        fiscal_year.company = company
        fiscal_year.save()
        return redirect(company.get_absolute_url())

fiscalyear_create = FiscaYearCreateView.as_view()


class FiscalYearUpdateView(UpdateView):
    model = models.FiscalYear
    form_class = forms.FiscalYearUpdateForm

    def get_context_data(self, **kwargs):
        context = super(FiscalYearUpdateView, self).get_context_data(**kwargs)
        context['company'] = self.object.company
        return context

fiscalyear_detail = FiscalYearUpdateView.as_view()


class FiscalYearDeleteView(DeleteView):
    model = models.FiscalYear

    def get_success_url(self):
        return self.object.company.get_absolute_url()

fiscalyear_delete = FiscalYearDeleteView.as_view()


#
# Accounts
#
class AccountListView(ListView):
    model = models.Account
    paginate_by = 20

accounts = AccountListView.as_view()


class AccountCreateView(CreateView):
    model = models.Account
    form_class = forms.AccountCreateForm


account_create = AccountCreateView.as_view()


class AccountUpdateView(UpdateView):
    model = models.Account

account_detail = AccountUpdateView.as_view()


class AccountDeleteView(DeleteView):
    model = models.Account
    success_url = reverse_lazy('account_list')

account_delete = AccountDeleteView.as_view()


#
# Ledgers
#
class LedgerListView(ListView):
    model = models.Ledger
    paginate_by = 20

ledgers = LedgerListView.as_view()


class LedgerCreateView(CreateView):
    model = models.Ledger
    success_url = reverse_lazy('ledger_list')

ledger_create = LedgerCreateView.as_view()


class LedgerUpdateView(UpdateView):
    model = models.Ledger

ledger_detail = LedgerUpdateView.as_view()


class LedgerDeleteView(DeleteView):
    model = models.Ledger
    success_url = reverse_lazy('ledger_list')

ledger_delete = LedgerDeleteView.as_view()


#
# Moves
#
class MoveListView(ListView):
    model = models.Move
    paginate_by = 20

moves = MoveListView.as_view()


class MoveCreateView(CreateView):
    model = models.Move

move_create = MoveCreateView.as_view()


class MoveUpdateView(UpdateView):
    model = models.Move

move_detail = MoveUpdateView.as_view()


class MoveDeleteView(DeleteView):
    model = models.Move
    success_url = reverse_lazy('move_list')

move_delete = MoveDeleteView.as_view()


#
# Move lines
#
class MoveLineCreateView(CreateView):
    model = models.MoveLine
    form_class = forms.MoveLineCreateForm

    def get_initial(self):
        self.move = models.Move.objects.get(pk=self.kwargs['pk'])
        return {'move': self.move}

    def get_context_data(self, **kwargs):
        self.move = models.Move.objects.get(pk=self.kwargs['pk'])
        context = super(MoveLineCreateView, self).get_context_data(**kwargs)
        context['move'] = self.move
        return context

    def form_valid(self, form):
        line = form.save(commit=False)
        line.move = self.move
        line.save()
        return redirect(self.move.get_absolute_url())

moveline_create = MoveLineCreateView.as_view()


class MoveLineUpdateView(UpdateView):
    model = models.MoveLine

    def get_context_data(self, **kwargs):
        self.move = models.Move.objects.get(pk=self.kwargs['pk'])
        context = super(MoveLineUpdateView, self).get_context_data(**kwargs)
        context['move'] = self.move
        return context

moveline_detail = MoveLineUpdateView.as_view()


class MoveLineDeleteView(DeleteView):
    model = models.MoveLine

    def get_success_url(self):
        return self.object.move.get_absolute_url()

moveline_delete = MoveLineDeleteView.as_view()
