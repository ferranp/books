
from django.conf.urls import patterns, url

urlpatterns = patterns('accounting.ui.views',
    url(r'^$', 'home', name="home"),
    # currencies
    url(r'^currencies/$', 'currencies', name="currency_list"),
    url(r'^currency/add/$', 'currency_create', name="currency_create"),
    url(r'^currency/(?P<pk>\d+)/$', 'currency_detail',
                                            name="currency_detail"),
    url(r'^currency/(?P<pk>\d+)/delete/$', 'currency_delete',
                                            name="currency_delete"),
    # companies
    url(r'^companies/$', 'companies', name="company_list"),
    url(r'^company/add/$', 'company_create', name="company_create"),
    url(r'^company/(?P<pk>\d+)/$', 'company_detail',
                                            name="company_detail"),
    url(r'^company/(?P<pk>\d+)/delete/$', 'company_delete',
                                            name="company_delete"),
    url(r'^company/(?P<pk>\d+)/accountchart/$', 'company_create_chart',
                                            name="company_create_chart"),

    url(r'^company/(?P<pk>\d+)/fiscalyear/add/$', 'fiscalyear_create',
                                            name="fiscalyear_create"),
    # Fiscal years
    url(r'^fiscalyear/(?P<pk>\d+)/$', 'fiscalyear_detail',
                                            name="fiscalyear_detail"),
    url(r'^fiscalyear/(?P<pk>\d+)/delete$', 'fiscalyear_delete',
                                            name="fiscalyear_delete"),
    # Accounts
    url(r'^accounts/', 'accounts', name="account_list"),
    url(r'^account/add/$', 'account_create', name="account_create"),
    url(r'^account/(?P<pk>\d+)/$', 'account_detail',
                                            name="account_detail"),
    url(r'^account/(?P<pk>\d+)/delete/$', 'account_delete',
                                            name="account_delete"),
    # Ledgers
    url(r'^ledgers/', 'ledgers', name="ledger_list"),
    url(r'^ledger/add/$', 'ledger_create', name="ledger_create"),
    url(r'^ledger/(?P<pk>\d+)/$', 'ledger_detail',
                                            name="ledger_detail"),
    url(r'^ledger/(?P<pk>\d+)/delete/$', 'ledger_delete',
                                            name="ledger_delete"),
    # Moves
    url(r'^moves/', 'moves', name="move_list"),
    url(r'^move/add/$', 'move_create', name="move_create"),
    url(r'^move/(?P<pk>\d+)/$', 'move_detail',
                                            name="move_detail"),
    url(r'^move/(?P<pk>\d+)/delete/$', 'move_delete',
                                            name="move_delete"),
    url(r'^move/(?P<pk>\d+)/line/add/$', 'moveline_create',
                                            name="moveline_create"),
    # Move lines
    url(r'^moveline/(?P<pk>\d+)/$', 'moveline_detail',
                                            name="moveline_detail"),
    url(r'^moveline/(?P<pk>\d+)/delete/$', 'moveline_delete',
                                            name="moveline_delete"),
)
