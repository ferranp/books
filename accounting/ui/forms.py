
import floppyforms as forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset

from accounting.models import Company, FiscalYear, MoveLine, Account
from accounting.chart_templates.models import AccountChartTemplate


class CompanyForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_tag = False
    helper.help_text_inline = True
    helper.layout = Layout(
            Fieldset('Company data',  # fieldset label
                Field('code', placeholder="", css_class="span2"),
                Field('name', placeholder="", css_class="span4"),
                'active', 'currency', 'logo', 'tags',
            ))

    class Meta:
        model = Company
        widgets = {
            'code': forms.TextInput(attrs={'class': 'span1'}),
            'name': forms.TextInput(attrs={'class': 'span4'}),
        }


class CompanyCreateChartForm(forms.Form):
    template = forms.ModelChoiceField(AccountChartTemplate.objects.all())


class FiscalYearCreateForm(forms.ModelForm):

    class Meta:
        exclude = ['company']
        model = FiscalYear


class FiscalYearUpdateForm(FiscalYearCreateForm):

    class Meta:
        exclude = ['company', 'year']
        model = FiscalYear


class MoveLineCreateForm(forms.ModelForm):

    class Meta:
        exclude = ['move', 'reconcile']
        model = MoveLine


class AccountCreateForm(forms.ModelForm):
    class Meta:
        model = Account
