from django.db import models
from django.utils.translation import ugettext_lazy as _
from accounting.models import Base, Company, Account, Ledger, Currency


class Bank(Base):
    code = models.CharField(max_length=10, verbose_name=_("Code"))
    name = models.CharField(max_length=100, verbose_name=_("Name"))
    company = models.ForeignKey(Company)
    ## *partner


class BankAccount(Base):
    bank = models.ForeignKey(Bank)
    name = models.CharField(max_length=100, verbose_name=_("Name"))
    account_number = models.CharField(max_length=30)
    iban = models.CharField(max_length=30, null=True, blank=True)

    account = models.ForeignKey(Account, related_name='banks',
                            null=True, blank=True)
    ledger = models.ForeignKey(Ledger, related_name='banks',
                            null=True, blank=True)
    costs_account = models.ForeignKey(Account, related_name='bank_costs',
                            null=True, blank=True)
    invoioce_ledger = models.ForeignKey(Ledger, related_name='bank_invoices',
                            null=True, blank=True)
    currency = models.ForeignKey(Currency, blank=True, null=True)

# BankStatment
   # *bank
   # initial_amount
   # final_amount
   # *move
#
# BankStatmentEntry
   # *statement
   # *account
   # *reference
   # *move_line
   # description
