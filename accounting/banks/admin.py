
from django.contrib import admin

from models import Bank, BankAccount


class BankAccountInline(admin.TabularInline):
    model = BankAccount
    fields = ('name', 'account_number', 'iban', 'account')


class BankAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code']
    list_display = ('name', 'code', 'company')
    list_filter = ("company",)

    inlines = [
        BankAccountInline,
    ]


class BankAccountAdmin(admin.ModelAdmin):
    search_fields = ['name', 'bank__name', 'bank__code']
    list_display = ('bank', 'name', 'account_number', 'account')
    list_filter = ('bank__company',)


admin.site.register(Bank, BankAdmin)
admin.site.register(BankAccount, BankAccountAdmin)
