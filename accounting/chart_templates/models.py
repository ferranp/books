from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from taggit.managers import TaggableManager
from mptt.models import MPTTModel, TreeForeignKey

from accounting.models import Base, AccountType, Account


class AccountChartTemplate(Base):
    name = models.CharField(max_length=100, verbose_name=_("Name"),
                                        unique=True)
    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.name

    def create(self, company):
        for line in self.lines.all():
            account = {
                'company': company,
                'code': line.code,
                'name': line.name,
                'type': line.type,
                'detail': line.detail,
                'reconciliable': line.reconciliable
            }
            if line.parent:
                account['parent'] = Account.objects.get(code=line.parent.code,
                                        company=company)

            Account.objects.create(**account)


class AccountChartTemplateLine(MPTTModel, Base):
    template = models.ForeignKey(AccountChartTemplate, related_name="lines")
    parent = TreeForeignKey('self', null=True, blank=True,
                                related_name='children')
    code = models.CharField(max_length=10, verbose_name=_("Code"), \
                blank=True, null=True)
    name = models.CharField(max_length=100, verbose_name=_("Name"), \
                blank=True, null=True)
    type = models.CharField(max_length=20,
            verbose_name=_("Account type"), default="auto")
    detail = models.BooleanField()
    reconciliable = models.BooleanField()

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.code

    class MPTTMeta:
        order_insertion_by = ['code']

    class Meta:
        ordering = ('template', 'code')
        unique_together = (('template', 'code'),)

    def clean(self):
        """ Validations """
        if not self.code.startswith(self.parent.code):
            raise ValidationError({'code': 'Incorrect code'})
