
from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from models import AccountChartTemplate, AccountChartTemplateLine


class AccountChartTemplateAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name',)


class AccountChartTemplateLineAdmin(MPTTModelAdmin):
    search_fields = ['code', 'name']
    list_display = ('code', 'template',  'name', 'type', 'detail',
                    'reconciliable')
    list_filter = ('template', 'type', 'detail')


admin.site.register(AccountChartTemplate, AccountChartTemplateAdmin)
admin.site.register(AccountChartTemplateLine, AccountChartTemplateLineAdmin)
