#!/bin/sh
if [ -z "$1" ]; then
  MODULES=
else
  MODULES=$1
fi
coverage run --branch manage.py test $MODULES
coverage report --include=accounting/\* --omit=local_settings.py,settings.py,manage.py,"*/migrations/*.py","*/wsgi.py"
coverage html --include=accounting/\*  --omit=local_settings.py,settings.py,manage.py,"*/migrations/*.py","*/wsgi.py"
#coverage xml --omit=local_settings.py,settings.py,manage.py,"*/migrations/*.py","*/wsgi.py"

